<?php

namespace Drupal\connection;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Connection type entities.
 */
interface ConnectionTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the description of the connection type.
   *
   * @return string
   */
  public function description();

  /**
   * Gets the description of the connection type.
   *
   * @param string $description
   *   The description to set.
   */
  public function setDescription($description);

  /**
   * Gets whether the connection is directed.
   *
   * @return bool
   *   TRUE if the connection is a directed connection.
   */
  public function isDirected();

  /**
   * Sets whether the connection is directed.
   *
   * @param bool $directed
   *   TRUE if the connection is directed.
   */
  public function setDirected($directed);

  /**
   * Gets whether the connection is commutative.
   *
   * @return bool
   *   TRUE if the connection is a commutative connection.
   */
  public function isCommutative();

  /**
   * Sets whether the connection is commutative.
   *
   * @param bool $commutative
   *   TRUE if the connection is commutative.
   */
  public function setCommutative($commutative);

  /**
   * Gets the label for the alpha label.
   *
   * @return string
   */
  public function alphaLabel();

  /**
   * Gets the label for the beta label.
   *
   * @return string
   */
  public function betaLabel();

  /**
   * Sets the label for the alpha entity field.
   *
   * @param string $label
   *   The label to set.
   */
  public function setAlphaLabel($label);

  /**
   * Sets the label for the beta entity field.
   *
   * @param string $label
   *   The label to set.
   */
  public function setBetaLabel($label);

}
