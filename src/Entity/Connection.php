<?php

namespace Drupal\connection\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\connection\ConnectionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Connection entity.
 *
 * @ingroup connection
 *
 * @ContentEntityType(
 *   id = "connection",
 *   label = @Translation("Connection"),
 *   bundle_label = @Translation("Connection type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\connection\ConnectionListBuilder",
 *     "views_data" = "Drupal\connection\Entity\ConnectionViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\connection\Form\ConnectionForm",
 *       "add" = "Drupal\connection\Form\ConnectionForm",
 *       "edit" = "Drupal\connection\Form\ConnectionForm",
 *       "delete" = "Drupal\connection\Form\ConnectionDeleteForm",
 *     },
 *     "access" = "Drupal\connection\ConnectionAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\connection\ConnectionHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "connection",
 *   admin_permission = "administer connection entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/connection/connection/{connection}",
 *     "add-form" = "/connection/connection/add/{connection_type}",
 *     "edit-form" = "/connection/connection/{connection}/edit",
 *     "delete-form" = "/connection/connection/{connection}/delete",
 *     "collection" = "/connection/connection",
 *   },
 *   bundle_entity_type = "connection_type",
 *   field_ui_base_route = "entity.connection_type.edit_form"
 * )
 */
class Connection extends ContentEntityBase implements ConnectionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'uid' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntities() {
  }

  /**
   * {@inheritdoc}
   */
  public function setEntities($entities) {
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity($delta) {
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity, $delta = 1) {
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Connection entity.'))
      ->setReadOnly(TRUE);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The Connection type/bundle.'))
      ->setSetting('target_type', 'connection_type')
      ->setRequired(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Connection entity.'))
      ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Connection entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Connection entity.'))
      ->setDisplayOptions('form', array(
        'type' => 'language_select',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    // @todo: These should be able to reference any entity type.
    // Waiting on dynamic entity references to land...
    // @see: https://www.drupal.org/node/2407587
    // @see: https://www.drupal.org/node/2427803
    // @see: https://www.drupal.org/node/2423093
    $fields['alpha'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Alpha Entity'))
      ->setDescription(t('The first entity to be connected.'))
      ->setSetting('target_type', 'node')
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['beta'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Beta Entity'))
      ->setDescription(t('The second entity to be connected.'))
      ->setSetting('target_type', 'node')
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
