<?php

namespace Drupal\connection\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\connection\ConnectionTypeInterface;

/**
 * Defines the Connection type entity.
 *
 * @ConfigEntityType(
 *   id = "connection_type",
 *   label = @Translation("Connection type"),
 *   handlers = {
 *     "list_builder" = "Drupal\connection\ConnectionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\connection\Form\ConnectionTypeForm",
 *       "edit" = "Drupal\connection\Form\ConnectionTypeForm",
 *       "delete" = "Drupal\connection\Form\ConnectionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\connection\ConnectionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "connection_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "connection",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/connection_type/{connection_type}",
 *     "add-form" = "/admin/structure/connection_type/add",
 *     "edit-form" = "/admin/structure/connection_type/{connection_type}/edit",
 *     "delete-form" = "/admin/structure/connection_type/{connection_type}/delete",
 *     "collection" = "/admin/structure/connection_type"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "directed",
 *     "commutative",
 *     "alpha_label",
 *     "beta_label",
 *   }
 * )
 */
class ConnectionType extends ConfigEntityBundleBase implements ConnectionTypeInterface {

  /**
   * The Connection type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Connection type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this node type.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether this connection type is directed.
   *
   * @var bool
   */
  protected $directed;

  /**
   * Whether this connection type is commutative.
   *
   * @var bool
   */
  protected $commutative;

  /**
   * The label for the alpha entity.
   *
   * @var string
   */
  protected $alpha_label;

  /**
   * The label for the beta entity.
   *
   * @var string
   */
  protected $beta_label;

  /**
   * {@intheritdoc}
   */
  public function description() {
    return $this->description;
  }

  /**
   * {@intheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@intheritdoc}
   */
  public function isDirected() {
    return $this->directed;
  }

  /**
   * {@intheritdoc}
   */
  public function setDirected($directed) {
    $this->directed = $directed;
  }

  /**
   * {@intheritdoc}
   */
  public function isCommutative() {
    return $this->commutative;
  }

  /**
   * {@intheritdoc}
   */
  public function setCommutative($commutative) {
    $this->commutative = $commutative;
  }

  /**
   * {@intheritdoc}
   */
  public function alphaLabel() {
    return $this->alpha_label;
  }

  /**
   * {@intheritdoc}
   */
  public function betaLabel() {
    return $this->beta_label;
  }

  /**
   * {@intheritdoc}
   */
  public function setAlphaLabel($label) {
    $this->alpha_label = $label;
  }

  /**
   * {@intheritdoc}
   */
  public function setBetaLabel($label) {
    $this->beta_label = $label;
  }

}
