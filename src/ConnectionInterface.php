<?php

namespace Drupal\connection;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Connection entities.
 *
 * @ingroup connection
 */
interface ConnectionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Connection type.
   *
   * @return string
   *   The Connection type.
   */
  public function getType();

  /**
   * Gets the Connection creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Connection.
   */
  public function getCreatedTime();

  /**
   * Sets the Connection creation timestamp.
   *
   * @param int $timestamp
   *   The Connection creation timestamp.
   *
   * @return \Drupal\connection\ConnectionInterface
   *   The called Connection entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the connected entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The connected entities.
   */
  public function getEntities();

  /**
   * Sets the connected entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface[]
   *   The entities to connect.
   */
  public function setEntities($entities);

  /**
   * Gets a connected entity by its delta.
   *
   * @param int $delta
   *   The delta of the entity to retrieve.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A connected entity.
   */
  public function getEntity($delta);

  /**
   * Gets a connected entity by its delta.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to set.
   * @param int $delta
   *   The delta of the entity to set.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   A connected entity.
   */
  public function setEntity(EntityInterface $entity, $delta = 1);

}
