<?php

namespace Drupal\connection\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConnectionTypeForm.
 *
 * @package Drupal\connection\Form
 */ class ConnectionTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $connection_type = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $connection_type->label(),
      '#description' => $this->t("Label for the connection type."),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $connection_type->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\connection\Entity\ConnectionType::load',
      ),
      '#disabled' => !$connection_type->isNew(),
    );

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#default_value' => $connection_type->description(),
      '#description' => $this->t("Description for the connection type."),
      '#required' => TRUE,
    );

    $form['directed'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Directed'),
      '#maxlength' => 255,
      '#default_value' => $connection_type->isDirected(),
      '#description' => $this->t("Whether this relationship should be directed."),
    );

    $form['commutative'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Commutative'),
      '#maxlength' => 255,
      '#default_value' => $connection_type->isCommutative(),
      '#description' => $this->t("Whether this relationship is commutative."),
    );

    $form['alpha_label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Alpha Label'),
      '#maxlength' => 255,
      '#default_value' => $connection_type->alphaLabel(),
      '#description' => $this->t("Overriden label for the alpha entity field."),
      '#required' => TRUE,
    );

    $form['beta_label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Beta Label'),
      '#maxlength' => 255,
      '#default_value' => $connection_type->betaLabel(),
      '#description' => $this->t("Overriden label for the beta entity field."),
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $connection_type = $this->entity;
    $status = $connection_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Connection type.', [
          '%label' => $connection_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Connection type.', [
          '%label' => $connection_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($connection_type->urlInfo('collection'));
  }

}
