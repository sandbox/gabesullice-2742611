<?php

namespace Drupal\connection\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Connection entities.
 *
 * @ingroup connection
 */
class ConnectionDeleteForm extends ContentEntityDeleteForm {


}
