<?php

/**
 * @file
 * Contains connection.page.inc.
 *
 * Page callback for Connection entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Connection templates.
 *
 * Default template: connection.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function connection_preprocess_connection(array &$variables) {
  // Fetch Connection Entity Object.
  $connection = $variables['elements']['#connection'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
* Prepares variables for a custom entity type creation list templates.
*
* Default template: connection-content-add-list.html.twig.
*
* @param array $variables
*   An associative array containing:
*   - content: An array of connection-types.
*
* @see block_content_add_page()
*/
function connection_preprocess_connection_content_add_list(&$variables) {
  $variables['types'] = array();
  $query = \Drupal::request()->query->all();
  foreach ($variables['content'] as $type) {
    $variables['types'][$type->id()] = array(
      'link' => Link::fromTextAndUrl($type->label(), new Url('entity.connection.add_form', array(
        'connection_type' => $type->id()
      ), array('query' => $query))),
      'description' => array(
      '#markup' => $type->label(),
      ),
      'title' => $type->label(),
      'localized_options' => array(
      'query' => $query,
      ),
    );
  }
}
